<?php
/* Copyright (C) 2018     Ambroise SALLAGOITY <ambroise.sallagoity@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

require_once DOL_DOCUMENT_ROOT.'/adherents/class/adherent.class.php';

class ActionsTxdoAdherent{
  protected $db;

  function ActionsTxdoAdherent($db){
    $this->db = $db;
  }

  function doActions($parameters, &$object, &$action, $hookmanager){
	  global $langs, $conf, $user, $bc;

    // $langs->load("societeinfo@societeinfo");
    
    if($action == 'search'){
        $nom =     GETPOST('nom', 'alpha');
        $prenom =  GETPOST('prenom', 'alpha');
        $ville =   GETPOST('ville', 'alpha');
        $tel =     GETPOST('tel', 'alpha');
        $email =   GETPOST('email', 'alpha');
        
        $this->displaySearchResult($nom, $prenom, $ville, $tel, $email);
    }

	  $type = GETPOST('type', 'alpha');

    if($action == "create" and !$type){

			$title=$langs->trans("Member");
			llxHeader('',$title);
			
      load_fiche_titre($langs->trans("NewMember"));

      print '<form action="'.$_SERVER["PHP_SELF"].'" method="post">';
			
			print 'Je souhaite <a href="'.$_SERVER['PHP_SELF'].'?action=create&type=auto">créer directement</a>. (sans recherche préalable)</p>';

			dol_fiche_head(null, 'card', '', 0, '');

			print '<table class="border" width="100%">';
			print '<tr><td class="fieldrequired">Nom</td>';
			print '<td><input type="text" name="nom"></td></tr>';
			
			print '<tr><td>Tél</td>';
			print '<td><input type="text" name="tel"><i>(la recherche s\'effectue sur les 3 champs)</i></td></tr>';
			
			print '<tr><td>Email</td>';
			print '<td><input type="text" name="email"></td></tr>';
			
			print '</table>';

			dol_fiche_end();

			print '<div class="center">';
			print '<button type="submit" class="button" name="action" value="search">'.$langs->trans('Search').'</button>';
			print '</div>';

			print '</form>';

			llxFooter();
			die();
		}
	}

    function displaySearchResult($nom, $prenom, $ville, $tel, $email){
        global $conf, $langs, $bc;
        
        try{
          
          $tel = $this->cleanupPhone($tel);

          $sql = "SELECT * FROM ".MAIN_DB_PREFIX."adherent"; $li = " WHERE ";
          // if ($prenom !== "") { $sql .= $li."UPPER(TRIM(firstname) = '".strtoupper($prenom)."'"; $li = " OR "; }
          if ($nom !== "")    { $sql .= $li."UPPER(TRIM(lastname))='".strtoupper($nom)."'"; $li = " OR "; }
          // if ($ville !== "")  { $sql .= $li."UPPER(town)='".strtoupper($ville)."'"; $li = " OR "; }
          if ($tel !== "")    { $sql .= $li."TRIM(phone)='".$tel."'"; $li = " OR "; }
          if ($tel !== "")    { $sql .= $li."TRIM(phone_perso)='".$tel."'"; $li = " OR "; }
          if ($tel !== "")    { $sql .= $li."TRIM(phone_mobile)='".$tel."'"; $li = " OR "; }
          if ($email !== "")  { $sql .= $li."TRIM(email)='".$email."'"; $li = " OR "; }
          
          $query = $this->db->query($sql);
            
        }catch(Exception $e){
          setEventMessage($langs->trans($e->getMessage()), 'errors');
          header('Location: '.$_SERVER["PHP_SELF"].'?action=create');
          exit;
        }
    
        $title=$langs->trans("NewMember");
        llxHeader('',$title, '', '', '', '', array('/txdoadherent/js/txdo_ajax.js.php'), '', 0, 0);
      
        print_fiche_titre($langs->trans("NewMember"));
    
        if($this->db->num_rows($query)==0){
    		print '<p>Aucun adhérent trouvé.</p>';
    		print '<input type="button" class="butAction" name="action" value="Créer nouveau" onClick="document.location.href = \''.$_SERVER["PHP_SELF"].'?action=create&type=auto'.'\'"></input>';
    		llxFooter();
    		die();
    	}
    
        print '<input type="hidden" name="nom"    value="'.$nom.'">';
        print '<input type="hidden" name="prenom" value="'.$prenom.'">';
        print '<input type="hidden" name="ville"  value="'.$ville.'">';
        print '<input type="hidden" name="tel"    value="'.$tel.'">';
        print '<input type="hidden" name="email"  value="'.$email.'">';
        
        print '<p>'.$this->db->num_rows($query).' adhérent(s) trouvé(s).</p>';
    
        print '<table class="border" width="100%">';
        print '<tr class="liste_titre"><td>Réf.</td><td>'.$langs->trans("Name").'</td><td>Prénom</td><td>Ville</td><td></td></tr>';
        $var = true;
        while($result = $this->db->fetch_array($query)){
          $var = !$var;
          print '<tr '.$bc[$var].'><td><a href=\''.$_SERVER["PHP_SELF"].'?rowid='.$result["rowid"].'\'>'.$result["rowid"].'</a></td><td>'.$result["lastname"].'</td><td>'.$result["firstname"].'</td><td>'.$result["town"].'</td><td><a href="#" data-id="'.$result["rowid"].'" class="moreinfo">Plus d\'info</a></td></tr>';
        }
        print '</table><br>';
    
        print '<div class="center">';
        print '<input type="button" class="butAction" name="action" value="Créer nouveau" onClick="document.location.href = \''.$_SERVER["PHP_SELF"].'?action=create&type=auto'.'\'"></input>';
        print '<input type="button" class="butAction" name="abort" value="Abandonner" onClick="document.location.href = \''.$_SERVER['PHP_SELF'].'?action=create'.'\'"></input>';
        print '</div>';
   
        llxFooter();
        die();
    }

    /** Nettoyage du numéro de téléphone. */
    private function cleanupPhone($phone)
    {
        // On retire d'abord tout ce qui n'est pas un chiffre
        $phone = preg_replace('/\D+/', '', $phone);

        // Vérification du nombre de chiffre dans le téléphone en prenant en
        // compte les cas +33601234578
        if (strlen($phone) == 10) {
            $phone = preg_replace('/(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/', '$1$2$3$4$5', $phone);
        } elseif (strlen($phone) == 11) {
            $phone = preg_replace('/(\d{2})(\d)(\d{2})(\d{2})(\d{2})(\d{2})/', '+$1$2$3$4$5$6', $phone);
        }

        return $phone;
    }

    /**
	 * Overloading the formObjectOptions function : replacing the parent's function with the one below
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	public function formObjectOptions($parameters, &$object, &$action)
	{
		global $conf, $user, $langs;

		$error = 0; // Error counter

        /* print_r($parameters); print_r($object); echo "action: " . $action; */
	    if (in_array($parameters['currentcontext'], array('membercard', 'somecontext1','somecontext2')))	    // do something only for the context 'somecontext1' or 'somecontext2'
	    {
			// Do what you want here...
			// You can for example call global vars like $fieldstosearchall to overwrite them, or update database depending on $action and $_POST values.
			print '<tr><td class="titlefield">'.$langs->trans("N° adhérent").'</td><td class="valeur">'.$object->ref.'&nbsp;</td></tr>';
		}

		// if (! $error) {
		// 	$this->results = array('myreturn' => 999);
		// 	$this->resprints = 'A text to show';
		// 	return 0; // or return 1 to replace standard code
		// } else {
		// 	$this->errors[] = 'Error message';
		// 	return -1;
		// }
	}
}
?>
