<?php
/* Copyright (C) 2018      Ambroise SALLAGOITY <ambroise.sallagoity@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

if(false === (@include '../../main.inc.php')) // From htdocs directory
    require '../../../main.inc.php'; // From "custom" directory
    
    $ajaxUrl = dol_buildpath('/txdoadherent/ajax/moreinfo.php', 1);
    header('Content-type: text/javascript; charset=UTF-8');
    ?>

$(function() {
	$("#dialogforpopup").dialog({
		autoOpen: false,
		minWidth: 500
	});

	$(".moreinfo").click(function(e){
		e.preventDefault();

		$("#dialogforpopup").empty();
		$("#dialogforpopup").append('<p>Chargement des informations en cours...</p>');
		$("#dialogforpopup").dialog('open');
		$.get('<?php echo $ajaxUrl ?>', {id: $(this).data('id')})
			.done(function(data){
					if(data.success){
    					$("#dialogforpopup").empty();
    					var table = $('<table class="border" width="100%">');
    					table.append('<tr><td colspan="2" class="liste_titre">Informations générales</td></tr>');
    					table.append('<tr><td>Nom</td><td>'+data.result.lastname+'</td></tr>');
    					table.append('<tr><td>Prenom</td><td>'+data.result.firstname+'</td></tr>');
    					table.append('<tr><td>Adresse</td><td>'+data.result.address+'</td></tr>');
    					table.append('<tr><td>Code postal</td><td>'+data.result.zip+'</td></tr>');
    					table.append('<tr><td>Ville</td><td>'+data.result.town+'</td></tr>');
    					table.append('<tr><td>Email</td><td>'+data.result.email+'</td></tr>');
    					table.append('<tr><td>Tel</td><td>'+data.result.phone+'</td></tr>');
						table.append('<tr><td>Tel perso</td><td>'+data.result.phone_perso+'</td></tr>');
						table.append('<tr><td>Tel mobile</td><td>'+data.result.phone_mobile+'</td></tr>');
    					table.append('</table>');
					    $("#dialogforpopup").append(table);
				}else{
					$.jnotify(data.errorMessage, 'error');
					$("#dialogforpopup").dialog("close");
				}
			})
			.fail(function(jqXHR, textStatus, errorThrown){
				$.jnotify(errorThrown, 'error');
			});
	});
});
